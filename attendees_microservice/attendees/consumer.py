from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO
from django.utils import dateparse


# Declare a function to update the AccountVO object (ch, method, properties, body)
def update_AccountVO(ch, method, properties, body):
    print("21")
    #   content = load the json in body
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    #   updated = convert updated_string from ISO string to datetime
    print("ran")
    updated = dateparse.parse_datetime(updated_string)
    #   if is_active:
    #       Use the update_or_create method of the AccountVO.objects QuerySet
    #           to update or create the AccountVO object
    if is_active:
        obj, created = AccountVO.objects.update_or_create(
            email=email,
            defaults={
                "first_name": first_name,
                "last_name": last_name,
                "updated": updated,
                "is_active": is_active,
            },
        )
    else:
        AccountVO.objects.filter(email=email).delete()


#   otherwise:
#       Delete the AccountVO object with the specified email, if it exists


# Based on the reference code at
#   https://github.com/rabbitmq/rabbitmq-tutorials/blob/master/python/receive_logs.py
# infinite loop
#   try
while True:
    try:
        print("55")
        #       create the pika connection parameters
        #       create a blocking connection with the parameters
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host="rabbitmq")
        )
        print("61")
        #       open a channel
        channel = connection.channel()
        #       declare a fanout exchange named "account_info"
        print("65")
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )
        print("66")
        #       declare a randomly-named queue
        #       get the queue name of the randomly-named queue
        result = channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue
        #       bind the queue to the "account_info" exchange
        print("72")
        channel.queue_bind(exchange="account_info", queue=queue_name)
        #       do a basic_consume for the queue name that calls
        #           function above
        #       tell the channel to start consuming
        print("77")
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_AccountVO,
            auto_ack=True,
        )
        print("connect to RabbitMQ")
        channel.start_consuming()
        #   except AMQPConnectionError

    except AMQPConnectionError:
        print("Cannot connect to RabbitMQ")
        time.sleep(5)

#       print that it could not connect to RabbitMQ
#       have it sleep for a couple of seconds
