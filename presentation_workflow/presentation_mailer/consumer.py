import json
import pika
import django
import os
import sys
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def accept_message(ch, method, properties, body):
    content = json.loads(body)
    print("accepted")
    send_mail(
        "Your presentation has been accepted",
        content["presenter_name"]
        + ", we're happy to tell you that your presentation "
        + content["title"]
        + " has been accepted",
        "admin@conference.go",
        [content["presenter_email"]],
        fail_silently=False,
    )


def reject_message(ch, method, properties, body):
    content = json.loads(body)
    print("rejected")
    send_mail(
        "Your presentation has been rejected",
        content["presenter_name"]
        + ", we're happy to tell you that your presentation "
        + content["title"]
        + " has been rejected",
        "admin@conference.go",
        [content["presenter_email"]],
        fail_silently=False,
    )


parameters = pika.ConnectionParameters(host="rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()

channel.queue_declare(queue="presentation_approval")
channel.basic_consume(
    queue="presentation_approval",
    on_message_callback=accept_message,
    auto_ack=True,
)

channel.queue_declare(queue="presentation_rejection")
channel.basic_consume(
    queue="presentation_rejection",
    on_message_callback=reject_message,
    auto_ack=True,
)
channel.start_consuming()
